<?php

/**
 * This files describe main code
 * Implementation of hook_menu
 * @return [type]
 */
function raw_sql_query_menu() {

  $rows = db_select('raw_sql_query', 'r')->fields('r')->execute()->fetchAll();

  foreach ($rows as $row) {
    $items[$row->menu_path] = array(
      'title' => $row->menu_title,
      'type' => MENU_CALLBACK,
      'page callback' => 'raw_sql_query_display',
      'page arguments' => array($row->tablename, $row->sql_query),
      'access callback' => 'user_access',
      'access arguments' => array('administer site configuration'),
    );
  }

    $items['rsq/download/%'] = array(
      'title' => t('Download'),
      'type' => MENU_CALLBACK,
      'page callback' => 'raw_sql_query_download',
      'page arguments' => array(2),
      'access callback' => 'user_access',
      'access arguments' => array('administer site configuration'),
    );

  return $items;
}

/**
 * Download a excel file
 * @param  [type] $tablename [description]
 * @return [type]            [description]
 */
function raw_sql_query_download($tablename) {

  $file = new RsqExportExcel();

  $sql = "SELECT * FROM " . raw_sql_query_prefix_table($tablename);

  $query = db_query($sql);

  $rows = $query->fetchAll();

 // cloning results to avoid warning messages when results contain just one row

  $rows2 = $rows;

  $header = array_shift($rows2);

  // it destroys the cloned version
  unset($rows2);

  $fields = array_keys((array) $header);

  $file->prepareFile($fields, $rows);

  $file->downloadFile($tablename);

}


/**
 * Display table of results
 * @param  [type] $tablename [description]
 * @param  [type] $sql_query [description]
 * @return [type]            [description]
 */
function raw_sql_query_display($tablename = NULL, $sql_query = NULL) {

  $data = array();
  $rows = array();

  //drupal_flush_all_caches();

  // starting select to display data in a table

  $query = db_select(raw_sql_query_prefix_table($tablename), 't')->fields('t')->extend('PagerDefault')->limit(50);

  $rows = $query->execute()->fetchAll();

  // cloning results to avoid warning messages when results contain just one row

  $rows2 = $rows;

  $header = array_shift($rows2);

  // it destroys the cloned version
  unset($rows2);

  $fields = array_keys((array) $header);

  foreach($rows as $row) {
    foreach($fields as $field) {
      $row_temp[] = $row->{$field};
    }
    $data[] = $row_temp;
    $row_temp = array();
  }

  $render = array(
    'table' => array(
      '#theme' => 'table',
      '#header' => $fields,
      '#rows' => $data,
      '#sticky' => TRUE,
      '#empty' => t('No rows recorded yet.'),
    ),
    'pager' => array(
      '#theme' => 'pager',
    ),
  );

   $selectors = drupal_get_form('raw_sql_query_form', $tablename);
   $output_selectors = drupal_render($selectors);
   $output_table = drupal_render($render);
   return $output_selectors . $output_table;
}


function raw_sql_query_form($form, $form_state, $tablename) {

  $fields = raw_sql_query_show_columns(raw_sql_query_prefix_table($tablename));
  $allowed_fields = db_select('raw_sql_query', 'r')->fields('r', array('fields'))->condition('r.tablename', $tablename)->execute()->fetchField();
  $allowed_fields = explode(',', $allowed_fields);

  // limiting number of selectors
  foreach ($fields as $k => $v) {
    if (!in_array($v->Field, $allowed_fields)) {
      unset($fields[$k]);
    }
  }

  foreach ($fields as $field) {
    $options = raw_sql_query_selector_builder(raw_sql_query_prefix_table($tablename), $field->Field);
    $options = array_merge($options, drupal_map_assoc(array('--not selected')));
    $form[$field->Field] = array(
      '#title' => $field->Field,
      '#type' => 'select',
      '#default_value' => '--not selected',
      '#options' => $options,
    );
  }
  $form['tablename'] = array(
    '#type' => 'hidden',
    '#value' => $tablename,
  );

  // $form['submit'] = array(
  //   '#type' => 'submit',
  //   '#value' => 'Submit',
  //   '#submit' => array('raw_sql_query_form_submit'),
  // );

  $form['download'] = array(
    '#type' => 'submit',
    '#value' => 'Download',
    '#submit' => array('raw_sql_query_form_download'),
  );
  return $form;
}


function raw_sql_query_form_submit($form, $form_state) {
  drupal_set_message('t');
}

function raw_sql_query_form_download($form, $form_state) {

  foreach ($form_state['values'] as $key => $value) {
    if ($value !== '--not selected') {
      $selected_fields[$key] = $value;
    }
  }

  // cleaning up standards fields

  unset($selected_fields['submit']);
  unset($selected_fields['download']);
  unset($selected_fields['form_build_id']);
  unset($selected_fields['form_token']);
  unset($selected_fields['form_id']);
  unset($selected_fields['op']);

  $tablename = $selected_fields['tablename'];
  unset($selected_fields['tablename']);

  // building query to generate excel file

  $file = new RsqExportExcel();

  $query = db_select(raw_sql_query_prefix_table($tablename), 't');;

  foreach ($selected_fields as $fieldname => $fieldvalue) {
      $query->condition('t.' . $fieldname, $fieldvalue);
    }
  $query->fields('t');
  $rows = $query->execute()->fetchAll();

 // cloning results to avoid warning messages when results contain just one row

  $rows2 = $rows;

  $header = array_shift($rows2);

  // it destroys the cloned version
  unset($rows2);

  $fields = array_keys((array) $header);

  $file->prepareFile($fields, $rows);

  $file->downloadFile($tablename);

  return array();
}

/**
 * Load mysql view schemas
 * @return [type] [description]
 */
function raw_sql_query_get_schemas() {
  $schemas = array();
  if (db_table_exists('raw_sql_query')) {
  $rows = db_select('raw_sql_query', 'r')->fields('r')->execute()->fetchAll();
  foreach ($rows as $row) {
    $schemas[$row->tablename] = schema_dbobject()->inspect(NULL, raw_sql_query_prefix_table($row->tablename));
  }
  }
  return $schemas;
}


/**
* Implementation of hook_ctools_plugin_directory().
*/
function raw_sql_query_ctools_plugin_directory($module, $type) {
  // Load the export_ui plugin.
  if ($type =='export_ui') {
    return 'plugins/export_ui';
  }
}

/**
 * It builds options for any selector
 * @param  [type] $tablename [description]
 * @param  [type] $fieldname [description]
 * @return [type]            [description]
 */
function raw_sql_query_selector_builder($tablename, $fieldname) {
  $query = db_select($tablename, 't');
  $query->fields('t', array($fieldname));
  $query->groupBy($fieldname);
  $rows = $query->execute()->fetchCol();
  return drupal_map_assoc($rows);
}


/**
 * Return columns of mysql view table
 * @param  [type] $tablename [description]
 * @return [type]            [description]
 */
function raw_sql_query_show_columns($tablename) {
  $fields = db_query("SHOW COLUMNS FROM " . $tablename)->fetchAll();
  return $fields;
}


/**
 * Add prefix rsq
 * @param  [type] $tablename [description]
 * @return [type]            [description]
 */
function raw_sql_query_prefix_table($tablename) {
  return "rsq_" . $tablename;
}

/**
 * create table
 * @param  [type] $tablename [description]
 * @return [type]            [description]
 */
function raw_sql_query_create_table($tablename, $sql_query) {
  try {
    db_query("CREATE TABLE IF NOT EXISTS  " . $tablename . " AS (" . str_replace(array("\"", "\'"), "'", $sql_query) . ")");

  } catch (Exception $e) {
    drupal_set_message(t('The table @tablename was not created. @error_message. If this query includes a reference to a table already created with this tool, you should add rsq_ as prefix, e.g. for "mytable" you should write "rsq_mytable"', array('@tablename' => $tablename, '@error_message' => $e->errorInfo[2])), 'error', FALSE);
  }
}

/**
 * drop table
 * @param  [type] $tablename [description]
 * @return [type]            [description]
 */
function raw_sql_query_drop_table($tablename) {
  try {
    db_drop_table($tablename);

  } catch (Exception $e) {
    drupal_set_message(t('The table @$tablename was not removed', array('@tablename' => $tablename)), 'error', FALSE);
  }
}
